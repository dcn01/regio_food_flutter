# regio_food

Regio Food is a simple app presenting list of Polish regional foods and details about them.

Data comes from Firebase server. In source code you can also find very simple example of handling JSON list.

## Getting Started

For help getting started with Flutter, view our online
[documentation](https://flutter.io/).
